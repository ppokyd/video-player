import { h, Component } from 'preact';

import Controls from './controls/index';

import style from './style';

export default class Player extends Component {
  setRef = (dom) => this.player = dom;

  state = {
    sources: [],
    playing: false,
    percent: 0,
    index: 0
  };

  constructor(props) {
    super();

    this.setState(props);

    this.onPlay = this.play.bind(this);
    this.onNext = this.next.bind(this);
    this.onPrev = this.prev.bind(this);
    this.onPause = this.pause.bind(this);
    this.onSeek = this.seek.bind(this);
    this.onPlaying = this.playing.bind(this);
  }

  componentDidMount() {
    console.log(this.player);

    this.player.addEventListener('timeupdate', (event) => {
      console.log('The currentTime attribute has been updated. Again.');
    });
  }
  
  next() {
    this.setState({
      index: this.getSourceIndex(1),
      playing: true
    });
    this.player.load();
    this.player.play();
  }

  prev() {
    this.setState({
      index: this.getSourceIndex(-1),
      playing: true
    });
    this.player.load();
    this.player.play();
  }

  pause() {
    this.setState({ playing: false });
    this.player.pause();
  }

  play() {
    this.setState({ playing: true });
    this.player.play();
  }

  playing() {
    const totalLength = this.player.duration;
    const percentageCompleted = (this.player.currentTime / totalLength) * 100;

    this.setState({
      percent: percentageCompleted
    });
  }

  seek(percent) {
    console.log(this.player.duration * (percent / 100));
    this.player.currentTime = this.player.duration * (percent / 100);
    this.setState({ percent });
    console.log(this.player.currentTime);
  }

  getSourceIndex(delta) {
    const { index, sources } = this.state;
    const newIndex = index + delta;

    if (newIndex > sources.length - 1) {
      return 0;
    }

    if (newIndex < 0) {
      return sources.length - 1;
    }

    return newIndex;
  }

  getSource() {
    const { index, sources } = this.state;

    return sources[index];
  }

  render() {
    const source = this.getSource();

    return (
      <div class={style.player}>
        <Controls
            playing={this.state.playing}
            onPlay={this.onPlay}
            onPause={this.onPause}
            onNext={this.onNext}
            onPrev={this.onPrev}
            onSeek={this.onSeek}
            percent={this.state.percent}
        />
        <video ref={this.setRef} onTimeUpdate={this.onPlaying}>
          {/* { this.props.sources.map(s => (<source src={s} />)) } */}
          <source src={source} />
        </video>
      </div>
    );
  }
}

