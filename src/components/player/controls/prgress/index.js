import { h, Component } from 'preact';
import style from './style';

export default class Progress extends Component {
  setRef = (dom) => this.bar = dom;

  constructor() {
    super();

    this.x = 0;
    this.width = 0;
    this.seek = this.seek.bind(this);
  }

  seek(e) {
    const x = e.pageX - this.x;
    this.props.onSeek(x / this.width * 100)
  }

  componentDidMount() {
    const { x, width } = this.bar.getBoundingClientRect();
    this.x = x;
    this.width = width;
  }

  render() {
    const { percent } = this.props;

    return (
      <div class={style.progress} ref={this.setRef} onClick={this.seek}>
        <div class={style.fill} style={{ width: percent + '%' }}></div>
        <div class={style.background}></div>
      </div>
    );
  }
}
