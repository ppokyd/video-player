import { h, Component } from 'preact';

import Progress from './prgress';

import style from './style';

export default class Controls extends Component {
  state = {
    shown: true
  };

  show() {
    this.setState({ shown: true });
  }

  hide() {
    this.setState({ shown: false });
  }

  render() {
    const { onPrev, onPause, onPlay, onNext, playing, percent, onSeek } = this.props;

    return (
      <div class={`${style.playerControls} ${this.state.shown ? style.shown : ''}`}
           onMouseEnter={this.show.bind(this)}
           onMouseLeave={this.hide.bind(this)}>
        <button class={style.prev} onClick={onPrev}>&nbsp;</button>
        <div class={style.playPause}>
          { playing ?
            <button onClick={onPause}>Pause</button> :
            <button onClick={onPlay}>Play</button>
          }
        </div>
        <button class={style.next} onClick={onNext}>&nbsp;</button>
        <Progress percent={percent} onSeek={onSeek.bind(this)} />
      </div>
    );
  }
}
